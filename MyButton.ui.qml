import QtQuick 2.12

Item {
    id: templateMyButton
    implicitWidth: 80
    implicitHeight: 41

    property alias mouseArea: mouseArea

    property alias event_textText: event_text.text
    property alias event_background_normal1Source: event_background_normal1.source
    property alias event_background_hover1Source: event_background_hover1.source
    property alias event_background_active1Source: event_background_active1.source

    Item {
        id: event_background_active
        anchors.fill: parent
        Image {
            id: event_background_active1
            anchors.fill: parent
            source: ""
            fillMode: Image.PreserveAspectFit
            sourceSize.height: 82
            sourceSize.width: 160
        }
    }

    Item {
        id: event_background_normal
        anchors.fill: parent
        Image {
            id: event_background_normal1
            anchors.fill: parent
            source: ""
            fillMode: Image.PreserveAspectFit
            sourceSize.height: 82
            sourceSize.width: 160
        }
    }

    Item {
        id: event_background_hover
        anchors.fill: parent
        Image {
            id: event_background_hover1
            anchors.fill: parent
            source: ""

            fillMode: Image.PreserveAspectFit
            sourceSize.height: 82
            sourceSize.width: 160
        }
    }

    Text {
        id: event_text
        color: "#5b5b5b"
        text: "BTN"
        anchors.fill: parent
        font.pixelSize: templateMyButton.height * 0.39

        maximumLineCount: 1
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        lineHeightMode: Text.ProportionalHeight
        wrapMode: Text.NoWrap
        font.capitalization: Font.AllUppercase
        fontSizeMode: Text.VerticalFit
        font.weight: Font.Bold
        font.family: "Inter"
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
    states: [
        State {
            id: stateButton
            name: "normal"
            when: !mouseArea.containsMouse && !mouseArea.containsPress
            PropertyChanges {
                target: event_text
                visible: true
            }

            PropertyChanges {
                target: event_background_hover
                visible: false
            }

            PropertyChanges {
                target: event_background_active
                visible: false
            }
        },
        State {
            name: "hover"
            when: mouseArea.containsMouse && !mouseArea.containsPress
            PropertyChanges {
                target: event_background_normal
                visible: false
            }

            PropertyChanges {
                target: event_background_active
                visible: false
            }
        },
        State {
            name: "active"
            when: mouseArea.containsPress
            PropertyChanges {
                target: event_background_hover
                visible: false
            }

            PropertyChanges {
                target: event_background_normal
                visible: false
            }

            PropertyChanges {
                target: event_text
                color: "#ffffff"
            }
        }
    ]
}

/*##^##
Designer {
    D{i:0;height:41;width:80}
}
##^##*/

