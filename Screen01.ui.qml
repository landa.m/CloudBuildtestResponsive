import QtQuick 2.8
import "."
import QtQuick.Studio.Components 1.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0

Rectangle {
    id: finalTestUIScreen
    width: 1920
    height: 1080
    color: "#f3f6fb"
    property alias jhonDoeText: jhonDoe.text
    property alias _000000Text: _000000.text
    property alias tLK1Text: tLK1.text
    property alias _000624Text: _000624.text
    property alias dOB02022002Text: dOB02022002.text
    property alias eCText: eC.text
    property alias toolsText: tools.text
    property alias mVT1Text: mVT1.text
    property alias _002133Text: _002133.text
    property alias eOText: eO.text
    property alias _001847Text: _001847.text
    property alias eC1Text: eC1.text
    property alias mVTText: mVT.text
    property alias eO1Text: eO1.text
    property alias eEGRecordText: eEGRecord.text
    property alias _001512Text: _001512.text
    property alias tLKText: tLK.text
    clip: true

    Item {
        id: ellipse4
        x: 1874
        y: 12
        width: 36
        height: 36
        Image {
            id: ellipse41
            x: 0
            y: 0
            source: "assets/Ellipse 41.svg"
        }
    }

    Item {
        id: ellipse5
        x: 1828
        y: 12
        width: 36
        height: 36
        Image {
            id: ellipse51
            x: 0
            y: 0
            source: "assets/Ellipse 51.svg"
        }
    }

    Item {
        id: ellipse6
        x: 1782
        y: 12
        width: 36
        height: 36
        Image {
            id: ellipse61
            x: 0
            y: 0
            source: "assets/Ellipse 61.svg"
        }
    }

    Item {
        id: ellipse7
        x: 15
        y: 12
        width: 36
        height: 36
        Image {
            id: ellipse71
            x: 0
            y: 0
            source: "assets/Ellipse 71.svg"
        }
    }

    Text {
        id: eEGRecord
        x: 61
        y: 21
        width: 104
        height: 18
        color: "#858585"
        text: "EEG Record"
        font.pixelSize: 16
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.capitalization: Font.AllUppercase
        font.weight: Font.Bold
        font.family: "Inter"
    }

    Item {
        id: group205
        x: 15
        y: 12
        width: 36
        height: 36
        Item {
            id: ellipse17
            x: 0
            y: 0
            width: 36
            height: 36
            Image {
                id: ellipse171
                x: 0
                y: 0
                source: "assets/Ellipse 171.svg"
            }
        }

        Rectangle {
            id: rectangle148
            x: 8
            y: 11
            width: 20
            height: 2
            color: "#ffffff"
            radius: 1
        }

        Rectangle {
            id: rectangle149
            x: 8
            y: 17
            width: 20
            height: 2
            color: "#ffffff"
            radius: 1
        }

        Rectangle {
            id: rectangle150
            x: 8
            y: 23
            width: 20
            height: 2
            color: "#ffffff"
            radius: 1
        }
    }

    Rectangle {
        id: rectangle158
        x: 1790
        y: 29
        width: 20
        height: 2
        color: "#bcbcbc"
        radius: 1
    }

    Item {
        id: group206
        x: 1884
        y: 22
        width: 16
        height: 16
        Rectangle {
            id: rectangle157
            x: -2
            y: 7
            width: 20
            height: 2
            color: "#bcbcbc"
            radius: 1
            rotation: -45
        }

        Rectangle {
            id: rectangle159
            x: -2
            y: 7
            width: 20
            height: 2
            color: "#bcbcbc"
            radius: 1
            rotation: 45
        }
    }

    Item {
        id: group207
        x: 1838
        y: 22
        width: 15
        height: 15
        SvgPathItem {
            id: rectangle155
            x: 7
            y: 0
            width: 8
            height: 8
            path: "M 6.610707714571618e-7 3.7808837890625 L 0 0 L 8.12210750579834 0 L 8.12210750579834 8.12210750579834 L 4.015865325927734 8.122108459472656"
            strokeWidth: 2
            strokeColor: "#524f73"
            fillColor: "transparent"
        }

        Rectangle {
            id: rectangle161
            x: 0
            y: 3
            width: 12
            height: 12
            color: "transparent"
            radius: 2
            border.color: "#524f73"
            border.width: 2
        }
    }

    Item {
        id: rectangle167
        x: 213
        y: 10
        width: 156
        height: 40
        Image {
            id: rectangle1671
            x: 0
            y: 0
            source: "assets/Rectangle 1671.svg"
        }
    }

    Item {
        id: doubleBanana
        x: 41
        y: 77
        width: 121
        height: 22
        Image {
            id: doubleBanana1
            x: 0
            y: 0
            source: "assets/Double Banana1.svg"
        }
    }

    Text {
        id: _000000
        x: 266
        y: 21
        width: 77
        height: 18
        color: "#5b5b5b"
        text: "00:00:00"
        font.pixelSize: 16
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.weight: Font.Bold
        font.family: "Inter"
    }

    SvgPathItem {
        id: rectangle168
        x: 170
        y: 83
        width: 7
        height: 7
        path: "M 6.660513877868652 0 L 6.660513877868652 6.660513877868652 L 0 6.660513877868652"
        strokeWidth: 2
        strokeColor: "#746ac2"
        fillColor: "transparent"
        rotation: 45
    }

    Item {
        id: hP1Hz
        x: 228
        y: 77
        width: 57
        height: 22
        Image {
            id: hP1Hz1
            x: 0
            y: 0
            source: "assets/HP 1Hz1.svg"
        }
    }

    Item {
        id: lP60Hz
        x: 352
        y: 77
        width: 67
        height: 22
        Image {
            id: lP60Hz1
            x: 0
            y: 0
            source: "assets/LP 60Hz1.svg"
        }
    }

    Item {
        id: sens90uVCm
        x: 488
        y: 77
        width: 118
        height: 22
        Image {
            id: sens90uVCm1
            x: 0
            y: 0
            source: "assets/Sens 90uV/Cm1.svg"
        }
    }

    Item {
        id: speed30mms
        x: 676
        y: 77
        width: 120
        height: 22
        Image {
            id: speed30mms1
            x: 0
            y: 0
            source: "assets/Speed 30mm/s1.svg"
        }
    }

    SvgPathItem {
        id: rectangle169
        x: 294
        y: 83
        width: 7
        height: 7
        path: "M 6.660513877868652 0 L 6.660513877868652 6.660513877868652 L 0 6.660513877868652"
        strokeWidth: 2
        strokeColor: "#746ac2"
        fillColor: "transparent"
        rotation: 45
    }

    SvgPathItem {
        id: rectangle170
        x: 430
        y: 83
        width: 7
        height: 7
        path: "M 6.660513877868652 0 L 6.660513877868652 6.660513877868652 L 0 6.660513877868652"
        strokeWidth: 2
        strokeColor: "#746ac2"
        fillColor: "transparent"
        rotation: 45
    }

    SvgPathItem {
        id: rectangle171
        x: 618
        y: 83
        width: 7
        height: 7
        path: "M 6.660513877868652 0 L 6.660513877868652 6.660513877868652 L 0 6.660513877868652"
        strokeWidth: 2
        strokeColor: "#746ac2"
        fillColor: "transparent"
        rotation: 45
    }

    SvgPathItem {
        id: rectangle172
        x: 807
        y: 83
        width: 7
        height: 7
        path: "M 6.660513877868652 0 L 6.660513877868652 6.660513877868652 L 0 6.660513877868652"
        strokeWidth: 2
        strokeColor: "#746ac2"
        fillColor: "transparent"
        rotation: 45
    }

    Rectangle {
        id: rectangle173
        x: 1799
        y: 70
        width: 100
        height: 32
        color: "#6fcf97"
        radius: 16
    }

    Item {
        id: notchOFF
        x: 1807
        y: 78
        width: 83
        height: 15
        Image {
            id: notchOFF1
            x: 0
            y: 0
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/Notch OFF1.svg"
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {
        id: patientBgr
        x: 1551
        y: 10
        width: 189
        height: 40
        color: "#f3f6fb"
        radius: 20
        border.color: "#e3e7ec"
        border.width: 2

        Item {
            id: patientAvatar
            y: 5
            width: 29
            height: 29
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: (parent.height - height) / 2

            Image {
                id: patient
                x: 0
                y: 0
                source: "assets/patient.svg"
                anchors.fill: parent
                sourceSize.width: parent.width * 8
                sourceSize.height: parent.height * 8

                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Rectangle {
        id: settingsBgr
        x: 1495
        y: 10
        width: 40
        height: 40
        color: "#f3f6fb"
        radius: 20
        border.color: "#e3e7ec"
        border.width: 2
    }

    Item {
        id: settingsBtn
        x: 1503
        y: 18
        width: 24
        height: 24

        Image {
            id: settingsIcon
            source: "assets/settingsBig.svg"
            cache: false
            mipmap: false
            smooth: true
            antialiasing: true
            anchors.fill: parent

            fillMode: Image.PreserveAspectFit
        }
    }

    Text {
        id: jhonDoe
        x: 1599
        y: 12
        width: 74
        height: 19
        color: "#5b5b5b"
        text: "Jhon Doe"
        font.pixelSize: 16
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.weight: Font.Medium
        font.family: "Inter"
    }

    Text {
        id: dOB02022002
        x: 1599
        y: 33
        width: 98
        height: 15
        color: "#bcbcbc"
        text: "DOB 02/02/2002"
        font.pixelSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.weight: Font.Normal
        font.family: "Inter"
    }

    Rectangle {
        id: rectangle176
        x: 160
        y: 927
        width: 127
        height: 73
        color: "#7e77b8"
        radius: 36.65002
    }


    Item {
        id: ellipse15
        x: 180
        y: 945
        width: 36
        height: 36
        Image {
            id: ellipse151
            x: 0
            y: 0
            source: "assets/Ellipse 151.svg"
        }
        rotation: 45
    }

    Item {
        id: group210
        x: 198
        y: 952
        width: 16
        height: 16
        Rectangle {
            id: rectangle1571
            x: -10
            y: 10
            width: 20
            height: 2
            color: "#746ac2"
            radius: 1
        }

        Rectangle {
            id: rectangle1591
            x: -10
            y: 10
            width: 20
            height: 2
            color: "#746ac2"
            radius: 1
            rotation: 90
        }
    }

    Item {
        id: group247
        x: 1421
        y: 70
        width: 331
        height: 31
    }

    RowLayout {
        id: toolButtons
        x: 1421
        y: 74

        Item {
            id: photoStimBtn
            Layout.preferredHeight: 22
            Layout.preferredWidth: 24

            Image {
                id: photoStimIcon
                x: 0
                y: 0
                width: parent.width
                height: parent.height
                source: "assets/photoStimIcon.svg"
                mipmap: false
                sourceSize.height: 600
                sourceSize.width: 600
                cache: false
                smooth: false
                antialiasing: false

                fillMode: Image.PreserveAspectFit
            }
        }

        Rectangle {
            id: rectangle206
            color: "#d2d2d2"
            Layout.preferredHeight: 1
            Layout.preferredWidth: 31
            rotation: 90
        }

        Image {
            id: impedance
            source: "assets/impedance.svg"
            Layout.preferredHeight: 24
            Layout.preferredWidth: 24
            smooth: false
            mipmap: true
            fillMode: Image.PreserveAspectFit
            sourceSize.height: 3700
            sourceSize.width: 3700
            cache: false
        }

        Rectangle {
            id: rectangle207
            color: "#d2d2d2"
            Layout.preferredHeight: 1
            Layout.preferredWidth: 31
            rotation: 90
        }

        Item {
            id: group1
            Layout.preferredHeight: 24
            Layout.preferredWidth: 24
            SvgPathItem {
                id: vector6
                x: 0
                y: 0
                width: 24
                height: 24
                path: "M 10.442487264205166 0 C 9.821702449150383 0 9.311772384400298 0.5099303682109704 9.311772384400298 1.1307151665921307 L 9.311772384400298 3.170436745155036 C 4.079443055257291 3.746879760896218 0 8.192142558727458 0 13.568582421981661 C 0 19.33301279083153 4.689142014693112 24.011070251464844 10.442487264205166 24.011070251464844 C 15.818927271864862 24.011070251464844 20.26418971335555 19.93162730577708 20.82954731183652 14.699298117168906 L 22.869269896655425 14.699298117168906 C 23.501140144453068 14.699298117168906 23.99998477646029 14.189367220362822 23.99998477646029 13.568582421981661 C 24.01107021994024 6.085908385493854 17.92516150166969 0 10.442487264205166 0 Z M 10.442487264205166 21.738554432975956 C 5.930711750521706 21.738554432975956 2.272515879526263 18.06927211774093 2.272515879526263 13.568582421981661 C 2.272515879526263 9.444797535075384 5.3431837622906295 6.030481170408758 9.311772384400298 5.4762090195578095 L 9.311772384400298 13.568582421981661 C 9.311772384400298 14.200452652807938 9.821702449150383 14.699298117168906 10.442487264205166 14.699298117168906 L 18.534860883981615 14.699298117168906 C 17.991674150986363 18.667886632686482 14.566272261871948 21.738554432975956 10.442487264205166 21.738554432975956 Z M 11.584288263926561 12.426781452927834 L 11.584288263926561 2.3279430335739844 C 16.905301055012444 2.8600443195347 21.15102650002435 7.105768804758727 21.683127800276743 12.426781452927834 L 11.584288263926561 12.426781452927834 Z"
                strokeWidth: 1
                strokeColor: "transparent"
                fillColor: "#7e77b8"
            }
        }

        Rectangle {
            id: rectangle2081
            color: "#d2d2d2"
            Layout.preferredHeight: 1
            Layout.preferredWidth: 31
            rotation: 90
        }

        SvgPathItem {
            id: vector7
            Layout.preferredHeight: 24
            Layout.preferredWidth: 27
            path: "M 25.722869300754006 3.3825624547167674 L 23.546343199175276 1.0219524023142055 C 22.376866822345804 -0.2449804285559678 20.362769954559237 -0.35326523602632764 19.074180307117395 0.8053827357392412 C 15.706521499414945 3.8048734155515733 13.194312328367257 6.024712872293229 11.331812863694758 7.681471237247374 C 5.906742154442252 12.500147467402268 5.906741986630974 12.500147467402268 5.7010007784039844 12.716717185611477 C 5.668515322774913 12.749202644633714 5.636029951051479 12.792516753505138 5.560230558886498 12.868316153587505 C 4.574838402653222 13.853708412746801 4.174184434834681 15.30472486383017 4.509867452669398 16.64745707541987 C 4.672294724360478 17.286337754463883 4.477382184214443 17.968532335842344 4.011757318046318 18.43415725064584 L 0.3409008125983457 22.061700030650087 C 0.0052177947636282695 22.3865546079639 -0.09223858503214745 22.863008979401652 0.09184566138116708 23.29614841582007 C 0.2651014204333643 23.72928785223849 0.676583778798824 24 1.1422086449669488 24 L 6.025855321786144 24 C 6.32905289044607 24 6.610593665103598 23.880885994066603 6.827163360691705 23.664316275857395 L 8.039953325525971 22.451526184344992 C 8.505578191694097 21.985901269541497 9.187772701816089 21.790987882888576 9.826653314127851 21.953415171545483 C 11.180213911553517 22.28909822444291 12.6204025111366 21.888444627849278 13.605794667369876 20.903052368689984 C 13.681594059534858 20.827252968607617 13.724908163882045 20.783939685884103 13.757393619511117 20.751454226861867 C 13.973963315099223 20.545712997144832 13.973964141247048 20.54571282933354 18.85761061152929 15.1964406243365 C 20.514368803432 13.377255073993938 22.734207460330744 10.951674064821217 25.722869300754006 7.681471237247374 C 26.859860202591566 6.4686808359295025 26.84903167650477 4.617009807201861 25.722869300754006 3.3825624547167674 Z M 5.636030034957118 21.661046382422214 L 5.560230558886498 21.736845453336272 L 3.903472470252265 21.736845453336272 L 4.813065356951878 20.84890993913768 L 5.636030034957118 21.661046382422214 Z M 6.708050193347811 16.106032779896847 C 6.599765345553758 15.68372181906204 6.664736256811901 15.2289254314764 6.87047746503889 14.860756900193897 L 11.613353302729731 19.603634059434732 C 11.234356335450544 19.809375289151767 10.790388469821774 19.874346207196243 10.368077553098118 19.76606134809164 C 9.350200025141408 19.50617767591374 8.256523021114077 19.657776179181567 7.336101814864625 20.16671499631951 L 6.296567441271278 19.127181340292804 C 6.816334731336429 18.206760037903667 6.967933838380387 17.123910414172734 6.708050193347811 16.106032779896847 Z M 24.055283305643847 6.176311530463791 L 13.23762601964053 18.001017814227424 L 8.48392186239985 13.24731316045362 L 20.579339030536474 2.5054550546620766 C 20.95833599781566 2.169772001764652 21.543074186230395 2.202257480149731 21.878757204065113 2.5704260114322346 L 22.550122929929113 3.2959344435108973 C 22.874977473311272 3.6424459978090558 23.3514317949825 4.162213161444998 24.055283305643847 4.93103565076084 C 24.380137849026006 5.277547205058998 24.380137849026006 5.8189715005820215 24.055283305643847 6.176311530463791 Z"
            strokeWidth: 1
            strokeColor: "transparent"
            fillColor: "#7e77b8"
        }

        Rectangle {
            id: rectangle2092
            color: "#d2d2d2"
            Layout.preferredHeight: 1
            Layout.preferredWidth: 31
            rotation: 90
        }

        SvgPathItem {
            id: vector8
            Layout.preferredHeight: 24
            Layout.preferredWidth: 24
            path: "M 20.901408450704224 0 L 3.0760562520631605 0 C 1.3746477583764307 0 0 1.3746475213064326 0 3.0760557215704254 L 0 16.090138414045246 C 0 17.780278967636136 1.3746477583764307 19.16619456544064 3.0760562520631605 19.16619456544064 L 5.791549467704666 19.16619456544064 L 5.791549467704666 20.91267313748456 C 5.791549467704666 22.60281369107545 7.166196796256052 23.988727569580078 8.867605289942782 23.988727569580078 L 15.132394710057218 23.988727569580078 C 16.82253555512764 23.988727569580078 18.208451391945424 22.61408133774855 18.208451391945424 20.91267313748456 L 18.208451391945424 19.154926274030085 L 20.923943318111796 19.154926274030085 C 22.61408416318222 19.154926274030085 24 17.78027832289868 24 16.07887012263469 L 24 3.0760557215704254 C 23.977464788396592 1.3746475213064326 22.602816944390955 0 20.901408450704224 0 Z M 3.0760562520631605 16.867602380977104 C 2.6478872433514664 16.867602380977104 2.2985915063132705 16.51830734891546 2.2985915063132705 16.090138414045246 L 2.2985915063132705 3.0760557215704254 C 2.2985915063132705 2.6478867867002123 2.6478872433514664 2.298591109901113 3.0760562520631605 2.298591109901113 L 10.839436275858274 2.298591109901113 L 10.839436275858274 16.878870672387663 L 3.0760562520631605 16.878870672387663 L 3.0760562520631605 16.867602380977104 Z M 15.909858811069542 20.901404846074005 C 15.909858811069542 21.32957378094422 15.560563718768911 21.67886881300586 15.132394710057218 21.67886881300586 L 8.856337856238996 21.67886881300586 C 8.428168847527303 21.67886881300586 8.078872895576584 21.32957378094422 8.078872895576584 20.901404846074005 L 8.078872895576584 19.154926274030085 L 15.909858811069542 19.154926274030085 L 15.909858811069542 20.901404846074005 Z M 21.690140845070424 16.090138414045246 C 21.690140845070424 16.51830734891546 21.340845752769795 16.867602380977104 20.9126767440581 16.867602380977104 L 13.149295430787852 16.867602380977104 L 13.149295430787852 2.2873234632280104 L 20.9126767440581 2.2873234632280104 C 21.340845752769795 2.2873234632280104 21.690140845070424 2.636619354939594 21.690140845070424 3.0647882898098073 L 21.690140845070424 16.090138414045246 Z"
            strokeWidth: 1
            strokeColor: "transparent"
            fillColor: "#7e77b8"
        }

        Rectangle {
            id: rectangle2102
            color: "#d2d2d2"
            Layout.preferredHeight: 1
            Layout.preferredWidth: 31
            rotation: 90
        }

        SvgPathItem {
            id: vector9
            Layout.preferredHeight: 18
            Layout.preferredWidth: 24
            path: "M 22.929248039427776 9.096978126317355 L 19.900856788937315 9.096978126317355 L 18.246056954136577 4.651732818684722 C 18.094637394190713 4.240736892062625 17.705273782153995 3.9595291442700287 17.26183078793253 3.9487134616936146 C 16.818387793711068 3.9487134616936146 16.418206523383493 4.197474152086995 16.24515559220538 4.597654406608483 L 14.947273627709468 7.615229965496837 L 13.130239372517869 0.7905341654973391 C 13.000451170910958 0.3146441289117971 12.54619247818007 -0.02064201756009533 12.048671068771275 0.0009893475927327407 C 11.551149659362482 0.02262071274556081 11.129337842973786 0.3903539367624474 11.031996698215252 0.8662439733479894 L 8.836412711541215 12.309236298748603 L 7.387111844657684 6.566108688700692 C 7.2681393286669 6.111850047889561 6.878774504660085 5.78737955609218 6.402884440696945 5.754932507557107 C 5.926994376733805 5.733301142404279 5.505182831104386 6.01450863797173 5.3429475855423965 6.447135934581642 L 4.164037969424384 9.583683835003505 L 1.0707525794505708 9.583683835003505 C 0.48670568511277135 9.583683835003505 0 10.059574026308626 0 10.654436559147255 C 0 11.249299091985884 0.48670568511277135 11.725189283291003 1.0707525794505708 11.725189283291003 L 4.899504333454955 11.725189283291003 C 5.342947327676419 11.725189283291003 5.75394321347631 11.443981370303026 5.905362773422174 11.032985443680928 L 6.132492364760297 10.416491876080237 L 7.927895996530981 17.511577725106417 C 8.046868512521765 17.98746776169196 8.479495666407518 18.32275390625 8.966201403093484 18.32275390625 L 8.998647957103204 18.32275390625 C 9.506985039234824 18.311938223673586 9.928796700903934 17.955020693816937 10.026137845662468 17.457499313030176 L 12.254169211517354 5.863089331709585 L 13.681839042394405 11.173589645147583 C 13.80081155838519 11.617032613858104 14.190175505647678 11.930686917822948 14.64443417259197 11.963133966358022 C 15.09869283953626 11.995581014893096 15.531320509153968 11.736004645952457 15.704371440332082 11.314193021443154 L 17.164488650389984 7.9072530606400955 L 18.137900123761916 10.535464217613745 C 18.28931968370778 10.957275842123048 18.700315982093237 11.227668057223822 19.1437589763147 11.227668057223822 L 22.929248039427776 11.227668057223822 C 23.524110606488403 11.227668057223822 24 10.7517778659187 24 10.156915333080072 C 24 9.572868472342051 23.524110606488403 9.096978126317355 22.929248039427776 9.096978126317355 Z"
            strokeWidth: 1
            strokeColor: "transparent"
            fillColor: "#7e77b8"
        }
    }

    Item {
        id: sigMarker1
        x: 374
        y: 112
        width: 19
        height: 908
        SvgPathItem {
            id: vector61
            x: 10
            y: 0
            width: 0
            height: 908
            path: "M 0 0 L 0 908.0050659179688"
            strokeWidth: 1
            strokeColor: "#fdb242"
            fillColor: "transparent"
        }

        Rectangle {
            id: rectangle200
            x: 0
            y: 882
            width: 19
            height: 16
            color: "#fdb242"
            radius: 3.02179
        }

        Text {
            id: eO1
            x: 3
            y: 883
            width: 16
            height: 13
            color: "#ffffff"
            text: "EO"
            font.pixelSize: 10
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode: Text.Wrap
            font.weight: Font.Bold
            font.family: "Inter"
        }
    }

    Item {
        id: sigMarker2
        x: 819
        y: 112
        width: 25
        height: 908
        SvgPathItem {
            id: vector71
            x: 13
            y: 0
            width: 0
            height: 908
            path: "M 0 0 L 0 908.0050659179688"
            strokeWidth: 1
            strokeColor: "#ff6767"
            fillColor: "transparent"
        }

        Rectangle {
            id: rectangle201
            x: 0
            y: 882
            width: 25
            height: 16
            color: "#ff6767"
            radius: 3.02179
        }

        Text {
            id: tLK1
            x: 3
            y: 883
            width: 21
            height: 13
            color: "#ffffff"
            text: "TLK"
            font.pixelSize: 10
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode: Text.Wrap
            font.weight: Font.Bold
            font.family: "Inter"
        }
    }

    Item {
        id: sigMarker3
        x: 478
        y: 112
        width: 29
        height: 908
        SvgPathItem {
            id: vector91
            x: 14
            y: 0
            width: 0
            height: 908
            path: "M 0 0 L 0 908.0050659179688"
            strokeWidth: 1
            strokeColor: "#36cc8d"
            fillColor: "transparent"
        }

        Rectangle {
            id: rectangle2093
            x: 0
            y: 882
            width: 29
            height: 16
            color: "#36cc8d"
            radius: 3.02179
        }

        Text {
            id: mVT1
            x: 3
            y: 883
            width: 24
            height: 13
            color: "#ffffff"
            text: "MVT"
            font.pixelSize: 10
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode: Text.Wrap
            font.weight: Font.Bold
            font.family: "Inter"
        }
    }

    Item {
        id: sigMarker4
        x: 1102
        y: 112
        width: 19
        height: 908
        SvgPathItem {
            id: vector81
            x: 9
            y: 0
            width: 0
            height: 908
            path: "M 0 0 L 0 908.0050659179688"
            strokeWidth: 1
            strokeColor: "#42bafd"
            fillColor: "transparent"
        }

        Rectangle {
            id: rectangle2082
            x: 0
            y: 882
            width: 19
            height: 16
            color: "#42bafd"
            radius: 3.02179
        }

        Text {
            id: eC1
            x: 3
            y: 883
            width: 15
            height: 13
            color: "#ffffff"
            text: "EC"
            font.pixelSize: 10
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode: Text.Wrap
            font.weight: Font.Bold
            font.family: "Inter"
        }
    }

    Item {
        id: ellipse1
        x: 232
        y: 22
        width: 16
        height: 16
        Image {
            id: ellipse11
            x: 0
            y: 0
            source: "assets/Ellipse 11.svg"
        }
    }

    Item {
        id: ellipse19
        x: 201
        y: 84
        width: 4
        height: 4
        Image {
            id: ellipse191
            x: 0
            y: 0
            source: "assets/Ellipse 191.svg"
        }
    }

    Item {
        id: ellipse20
        x: 325
        y: 84
        width: 4
        height: 4
        Image {
            id: ellipse201
            x: 0
            y: 0
            source: "assets/Ellipse 201.svg"
        }
    }

    Item {
        id: ellipse21
        x: 461
        y: 84
        width: 4
        height: 4
        Image {
            id: ellipse211
            x: 0
            y: 0
            source: "assets/Ellipse 211.svg"
        }
    }

    Item {
        id: ellipse22
        x: 651
        y: 84
        width: 4
        height: 4
        Image {
            id: ellipse221
            x: 0
            y: 0
            source: "assets/Ellipse 221.svg"
        }
    }

    Image {
        id: settingsBig
        x: 1384
        y: 147
        width: slider.value
        anchors.verticalCenter: parent.verticalCenter
        source: "images/settingsBig.svg"
        sourceSize.height: 800
        sourceSize.width: 800
        anchors.horizontalCenter: parent.horizontalCenter
        antialiasing: false
        mipmap: false
        smooth: true
        cache: false
        fillMode: Image.PreserveAspectFit
    }
    Image {
        id: settingsSmall
        x: 408
        y: 147
        width: slider.value
        height: slider.value
        anchors.verticalCenter: parent.verticalCenter
        source: "images/settings.svg"
        sourceSize.height: 800
        sourceSize.width: 800
        anchors.verticalCenterOffset: 0
        antialiasing: false
        mipmap: false
        smooth: true
        cache: false
        fillMode: Image.PreserveAspectFit
    }

    Text {
        id: tools
        x: 225
        y: 954
        width: 44
        height: 19
        color: "#ffffff"
        text: "Tools"
        font.pixelSize: 16
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.Wrap
        font.weight: Font.Bold
        font.family: "Inter"
    }

    ColumnLayout {
        anchors.fill: parent

        Item {
            id: header
            Layout.minimumWidth: 600
            Layout.minimumHeight: 50
            Layout.preferredWidth: 1920
            Layout.preferredHeight: 60
            Layout.fillHeight: true
            Layout.fillWidth: true
            Image {
                id: rectangle1471
                anchors.fill: parent
                source: "assets/Rectangle 1471.svg"

                Slider {
                    id: slider
                    x: 1040
                    y: 15
                    to: 800
                    from: 10
                    value: 24
                }
            }
        }

        Rectangle {
            id: toolBar
            color: "#f3f6fb"
            Layout.minimumHeight: 40
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: 40
            Layout.preferredWidth: 1920
        }

        RowLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true

            Item {
                id: sideLeft
                Layout.minimumWidth: 80
                Layout.minimumHeight: 300
                Layout.preferredHeight: 908
                Layout.preferredWidth: 100
                Image {
                    id: rectangle1651
                    anchors.fill: parent
                    source: "assets/Rectangle 1651.svg"

                    ColumnLayout {
                        id: sig
                        anchors.fill: parent

                        ColumnLayout {
                            id: sig1
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                            Item {
                                id: fp1F7
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Text {
                                    text: "fp1-F7"
                                    font.bold: true
                                    font.pointSize: 15
                                    maximumLineCount: 1
                                    fontSizeMode: Text.Fit
                                    font.capitalization: Font.AllUppercase
                                    x: 0
                                    y: 0
                                }
                            }

                            Item {
                                id: f7T3
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: f7T31
                                    x: 0
                                    y: 0
                                    source: "assets/F7-T31.svg"
                                }
                            }

                            Item {
                                id: t3T5
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: t3T51
                                    x: 0
                                    y: 0
                                    source: "assets/T3-T51.svg"
                                }
                            }

                            Item {
                                id: t5O1
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: t5O11
                                    x: 0
                                    y: 0
                                    source: "assets/T5-O11.svg"
                                }
                            }
                        }

                        ColumnLayout {
                            id: sig2
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                            Item {
                                id: fp1E3
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: fp1E31
                                    x: 0
                                    y: 0
                                    source: "assets/Fp1-E31.svg"
                                }
                            }

                            Item {
                                id: f3C3
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: f3C31
                                    x: 0
                                    y: 0
                                    source: "assets/F3-C31.svg"
                                }
                            }

                            Item {
                                id: c3P3
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: c3P31
                                    x: 0
                                    y: 0
                                    source: "assets/C3-P31.svg"
                                }
                            }

                            Item {
                                id: p3O1
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: p3O11
                                    x: 0
                                    y: 0
                                    source: "assets/P3-O11.svg"
                                }
                            }
                        }

                        ColumnLayout {
                            id: sig3
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                            Item {
                                id: fp2F8
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: fp2F81
                                    x: 0
                                    y: 0
                                    source: "assets/Fp2-F81.svg"
                                }
                            }

                            Item {
                                id: f8T4
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 56
                                Image {
                                    id: f8T41
                                    x: 0
                                    y: 0
                                    source: "assets/F8-T41.svg"
                                }
                            }

                            Item {
                                id: t4T6
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: t4T61
                                    x: 0
                                    y: 0
                                    source: "assets/T4-T61.svg"
                                }
                            }

                            Item {
                                id: t6O2
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: t6O21
                                    x: 0
                                    y: 0
                                    source: "assets/T6-O21.svg"
                                }
                            }
                        }

                        ColumnLayout {
                            id: sig4
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                            Item {
                                id: fp2F4
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: fp2F41
                                    x: 0
                                    y: 0
                                    source: "assets/Fp2-F41.svg"
                                }
                            }

                            Item {
                                id: f4C4
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: f4C41
                                    x: 0
                                    y: 0
                                    source: "assets/F4-C41.svg"
                                }
                            }

                            Item {
                                id: c4P4
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 64
                                Image {
                                    id: c4P41
                                    x: 0
                                    y: 0
                                    source: "assets/C4-P41.svg"
                                }
                            }

                            Item {
                                id: p4O2
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 64
                                Image {
                                    id: p4O21
                                    x: 0
                                    y: 0
                                    source: "assets/P4-O21.svg"
                                }
                            }
                        }

                        ColumnLayout {
                            id: sig5
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                            Item {
                                id: p4O22
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 64
                                Image {
                                    id: p4O211
                                    x: 0
                                    y: 0
                                    source: "assets/P4-O21_1.svg"
                                }
                            }

                            Item {
                                id: p4O23
                                Layout.preferredHeight: 18
                                Layout.preferredWidth: 65
                                Image {
                                    id: p4O212
                                    x: 0
                                    y: 0
                                    source: "assets/P4-O21_2.svg"
                                }
                            }

                            Item {
                                id: eKG
                                Layout.preferredHeight: 19
                                Layout.preferredWidth: 65
                                Image {
                                    id: eKG1
                                    x: 0
                                    y: 0
                                    source: "assets/EKG1.svg"
                                }
                            }
                        }
                    }
                }
            }

            Item {
                id: bgrSig
                Layout.minimumHeight: 600
                Layout.minimumWidth: 100
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.preferredHeight: 908
                Layout.preferredWidth: 1640
                Image {
                    id: rectangle1751
                    anchors.fill: parent
                    source: "assets/Rectangle 1751.svg"
                }
            }

            Item {
                id: eventsRight
                Layout.minimumHeight: 600
                Layout.minimumWidth: 95
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.preferredHeight: 908
                Layout.preferredWidth: 100
                Image {
                    id: eventsBackground1
                    anchors.fill: parent
                    source: "assets/eventsBackground1.svg"
                }

                ColumnLayout {

                    ColumnLayout {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        rotation: 0

                        EventButton {
                            id: event_button_1
                            Layout.preferredHeight: 41
                            Layout.preferredWidth: 80
                            event_textText: "mvt"
                        }

                        EventButton {
                            id: event_button_2
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "EO"
                        }

                        EventButton {
                            id: event_button_3
                            Layout.preferredHeight: 41
                            Layout.preferredWidth: 80
                            event_textText: "ec"
                        }

                        EventButton {
                            id: event_button_4
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "ywn"
                        }

                        EventButton {
                            id: event_button_5
                            Layout.preferredHeight: 41
                            Layout.preferredWidth: 80
                            event_textText: "art"
                        }

                        EventButton {
                            id: event_button_6
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "cgh"
                        }

                        EventButton {
                            id: event_button_7
                            Layout.preferredHeight: 41
                            Layout.preferredWidth: 80
                            event_textText: "tlk"
                        }

                        EventButton {
                            id: event_button_8
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "lm"
                        }

                        EventButton {
                            id: event_button_9
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "snr"
                        }

                        EventButton {
                            id: event_button_10
                            Layout.preferredHeight: 41
                            Layout.preferredWidth: 80
                            event_textText: "rok"
                        }

                        EventButton {
                            id: event_button_11
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "cust"
                        }

                        EventButton {
                            id: event_button_12
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "HV 30s"
                        }

                        EventButton {
                            id: event_button_13
                            Layout.preferredHeight: 42
                            Layout.preferredWidth: 80
                            event_textText: "HV 15s"
                        }
                    }

                    Item {
                        id: rectangle209
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.preferredHeight: 215
                        Layout.preferredWidth: 92
                        Image {
                            id: rectangle2091
                            anchors.fill: parent
                            source: "assets/Rectangle 2091.svg"

                            ColumnLayout {
                                anchors.fill: parent

                                Item {
                                    id: group243
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    Layout.preferredHeight: 39
                                    Layout.preferredWidth: 76
                                    Text {
                                        id: _002133
                                        x: 0
                                        y: 21
                                        width: 77
                                        height: 18
                                        color: "#bcbcbc"
                                        text: "00:21:33"
                                        font.pixelSize: 14
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }

                                    Rectangle {
                                        id: rectangle2101
                                        x: 29
                                        y: 0
                                        width: 19
                                        height: 16
                                        color: "#fdb242"
                                        radius: 3.02179
                                    }

                                    Text {
                                        id: eO
                                        x: 31
                                        y: 2
                                        width: 15
                                        height: 13
                                        color: "#ffffff"
                                        text: "EO"
                                        font.pixelSize: 10
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }
                                }

                                Item {
                                    id: group244
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    Layout.preferredHeight: 39
                                    Layout.preferredWidth: 76
                                    Text {
                                        id: _001847
                                        x: 0
                                        y: 21
                                        width: 77
                                        height: 18
                                        color: "#bcbcbc"
                                        text: "00:18:47"
                                        font.pixelSize: 14
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }

                                    Rectangle {
                                        id: rectangle211
                                        x: 24
                                        y: 0
                                        width: 29
                                        height: 16
                                        color: "#36cc8d"
                                        radius: 3.02179
                                    }

                                    Text {
                                        id: mVT
                                        x: 26
                                        y: 2
                                        width: 25
                                        height: 13
                                        color: "#ffffff"
                                        text: "MVT"
                                        font.pixelSize: 10
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }
                                }

                                Item {
                                    id: group245
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    Layout.preferredHeight: 39
                                    Layout.preferredWidth: 76
                                    Text {
                                        id: _001512
                                        x: 0
                                        y: 21
                                        width: 77
                                        height: 18
                                        color: "#bcbcbc"
                                        text: "00:15:12"
                                        font.pixelSize: 14
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }

                                    Rectangle {
                                        id: rectangle212
                                        x: 26
                                        y: 0
                                        width: 25
                                        height: 16
                                        color: "#ff6767"
                                        radius: 3.02179
                                    }

                                    Text {
                                        id: tLK
                                        x: 28
                                        y: 2
                                        width: 21
                                        height: 13
                                        color: "#ffffff"
                                        text: "TLK"
                                        font.pixelSize: 10
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }
                                }

                                Item {
                                    id: group246
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    Layout.preferredHeight: 41
                                    Layout.preferredWidth: 76
                                    Text {
                                        id: _000624
                                        x: 0
                                        y: 23
                                        width: 77
                                        height: 18
                                        color: "#bcbcbc"
                                        text: "00:06:24"
                                        font.pixelSize: 14
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }

                                    Rectangle {
                                        id: rectangle213
                                        x: 29
                                        y: 0
                                        width: 19
                                        height: 16
                                        color: "#42bafd"
                                        radius: 3.02179
                                    }

                                    Text {
                                        id: eC
                                        x: 31
                                        y: 2
                                        width: 15
                                        height: 13
                                        color: "#ffffff"
                                        text: "EC"
                                        font.pixelSize: 10
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignTop
                                        wrapMode: Text.Wrap
                                        font.weight: Font.Bold
                                        font.family: "Inter"
                                    }
                                }
                            }
                        }
                    }
                }
                rotation: 0
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.33;uuid:"bc7b0c70-7356-5722-bcbc-a9e32975173d"}D{i:2;uuid:"5c397327-a83b-5d96-95da-6b23ac9aafad-1"}
D{i:1;uuid:"5c397327-a83b-5d96-95da-6b23ac9aafad"}D{i:4;uuid:"069b976e-953a-5bd8-94f0-c8457df85573-1"}
D{i:3;uuid:"069b976e-953a-5bd8-94f0-c8457df85573"}D{i:6;uuid:"61296e88-d414-53c8-9526-d5547be19bda-1"}
D{i:5;uuid:"61296e88-d414-53c8-9526-d5547be19bda"}D{i:8;uuid:"03a11cd5-af16-5612-9e67-187d8e9a7ee6-1"}
D{i:7;uuid:"03a11cd5-af16-5612-9e67-187d8e9a7ee6"}D{i:9;uuid:"9e724508-9afe-5920-93f6-8b20479c50b0"}
D{i:12;uuid:"53b93181-3273-5f5e-8cfa-33475cf5abac-1"}D{i:11;uuid:"53b93181-3273-5f5e-8cfa-33475cf5abac"}
D{i:13;uuid:"110f5d8c-5d30-531b-b778-2aca5c38a693"}D{i:14;uuid:"dac85b4b-8afe-559c-8ea0-f989f2c9cc1b"}
D{i:15;uuid:"fc54a9e4-b187-5b29-8b15-06bb5b4b8f2c"}D{i:10;uuid:"36be5267-7276-5f13-985d-68d63eaca3bf"}
D{i:16;uuid:"66fae9aa-5827-5d47-889e-06982c06ad56"}D{i:18;uuid:"ae110756-63e9-54d7-a322-cac811537b77"}
D{i:19;uuid:"c42fded0-93af-5754-b61a-ec790764bee6"}D{i:17;uuid:"bb3702d0-bbab-58ab-9295-29bc2d33f657"}
D{i:21;uuid:"722dac2b-78cf-50f1-85a5-466832c7419b"}D{i:22;uuid:"4a44478e-12a2-5eac-bf19-9707dc8ffe89"}
D{i:20;uuid:"3437b632-4aea-5623-a304-917873871095"}D{i:24;uuid:"18ee387a-0db4-5052-98a6-71101653876e-1"}
D{i:23;uuid:"18ee387a-0db4-5052-98a6-71101653876e"}D{i:26;uuid:"a7f60563-3023-513f-a507-278122275a67-1"}
D{i:25;uuid:"a7f60563-3023-513f-a507-278122275a67"}D{i:27;uuid:"ef4602bd-d182-57f6-88cb-fca904d3dd1e"}
D{i:28;uuid:"59bf7de1-8ab6-5bac-9c81-286621dba29c"}D{i:30;uuid:"9978f636-f18a-51a7-86b9-c9ba62d426b8-1"}
D{i:29;uuid:"9978f636-f18a-51a7-86b9-c9ba62d426b8"}D{i:32;uuid:"6d3a9179-945f-54db-98fe-9f96486eb357-1"}
D{i:31;uuid:"6d3a9179-945f-54db-98fe-9f96486eb357"}D{i:34;uuid:"1183c034-c4a5-5ed9-9f2e-13a108e6906c-1"}
D{i:33;uuid:"1183c034-c4a5-5ed9-9f2e-13a108e6906c"}D{i:36;uuid:"bbcf08e6-1aef-517b-9e5d-b58f9cf56e3b-1"}
D{i:35;uuid:"bbcf08e6-1aef-517b-9e5d-b58f9cf56e3b"}D{i:37;uuid:"fca4d525-2cac-5d53-a981-4631ef7d7c23"}
D{i:38;uuid:"625cc2ae-faf4-53a7-92c9-283160d61557"}D{i:39;uuid:"cfbb7cee-4cb3-59ec-8c05-a27ada3e91ba"}
D{i:40;uuid:"43e3d4e0-9a91-59ec-a981-7efd2d1d4d6c"}D{i:41;uuid:"94206b40-19bd-5b88-aafb-1ce5d4dfd433"}
D{i:43;uuid:"b57a3716-70e2-5f16-97e0-c7cdf6ef3619-1"}D{i:42;uuid:"b57a3716-70e2-5f16-97e0-c7cdf6ef3619"}
D{i:45;uuid:"2fcb7cce-15b9-58ba-a53e-3f37bd4a2831"}D{i:48;uuid:"a231058d-e905-5c7c-9bd4-3798ab5a83f3"}
D{i:50;uuid:"d92673f1-c8a2-5760-bbe0-b868924f3919"}D{i:51;uuid:"541b14fb-8968-5e8b-9580-7cf5fd91e869"}
D{i:52;uuid:"0d7d4da6-66c9-5bae-8e43-7857e34579bc"}D{i:54;uuid:"b07380fd-a5a4-5a6c-a91d-638b6bcf4c87-1"}
D{i:53;uuid:"b07380fd-a5a4-5a6c-a91d-638b6bcf4c87"}D{i:56;uuid:"a510a585-2ce4-548b-8602-c33a2139bcf2"}
D{i:57;uuid:"1bab5bf4-a0a2-54ba-9b76-b383f7644ec6"}D{i:55;uuid:"91636d04-bcc0-5cad-8cc3-182f6878ce1c"}
D{i:58;uuid:"9b0c22d6-1750-52ca-8a94-c03d4fe3dac1"}D{i:62;uuid:"30f47b90-8543-54a9-bf4e-7c246284da28"}
D{i:64;uuid:"a49edd9c-8eaa-5356-b605-b90f551c1eef"}D{i:67;uuid:"dcac267a-c1cb-5831-b010-ab37552240ed"}
D{i:68;uuid:"812d7766-f48a-51f7-848c-e3b2acd2a20e"}D{i:69;uuid:"27b2d9c2-dcf6-5378-af7a-738e3fc2f797"}
D{i:70;uuid:"339e745e-bf45-5791-83e1-246b6ee33d6d"}D{i:74;uuid:"5bda0663-b312-5818-9f9a-e3f822398727"}
D{i:75;uuid:"97f8c74f-9c6e-5f45-9e25-928660727c5b"}D{i:76;uuid:"934a57cd-3b4d-5597-bd4c-ea3f9dfeeb53"}
D{i:73;uuid:"3fc2286c-5a00-529e-8970-dfeb63cb0bad"}D{i:77;uuid:"458420dd-11ec-565a-ab37-1fd5622ad2b4"}
D{i:81;uuid:"2859d9a5-50f8-589c-b3fa-6986e90dad54"}D{i:85;uuid:"2c5daf70-61ea-56b5-ba9c-884c33d1c7e0"}
D{i:90;uuid:"97212efc-7f1f-5ef5-9364-24daa493dd12-1"}D{i:89;uuid:"97212efc-7f1f-5ef5-9364-24daa493dd12"}
D{i:92;uuid:"918b7e65-7cc5-51f1-85f7-2d6b64c53499-1"}D{i:91;uuid:"918b7e65-7cc5-51f1-85f7-2d6b64c53499"}
D{i:104;uuid:"a164412a-ffef-5b3a-a1b4-0f4e53046362-1"}D{i:103;uuid:"a164412a-ffef-5b3a-a1b4-0f4e53046362"}
D{i:102}
}
##^##*/

