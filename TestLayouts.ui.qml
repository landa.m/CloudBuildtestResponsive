import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Item {
    width: 1440
    height: 900

    Rectangle {
        id: rectangle
        color: "#bdc7db"
        border.width: 0
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        ColumnLayout {
            anchors.fill: parent

            Rectangle {
                id: rectangle1
                color: "#ffffff"
                radius: 5
                Layout.maximumHeight: 60
                Layout.minimumHeight: 50
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.preferredHeight: 60
                Layout.preferredWidth: 1920
            }

            Rectangle {
                id: rectangle2
                color: "#e3e7ec"
                Layout.minimumHeight: 40
                Layout.maximumHeight: 60
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.preferredHeight: 50
                Layout.preferredWidth: 1920
            }

            RowLayout {
                Layout.fillHeight: true
                Layout.fillWidth: true

                Rectangle {
                    id: rectangle3
                    color: "#ffffff"
                    radius: 20
                    Layout.leftMargin: 5
                    Layout.rightMargin: 0
                    Layout.maximumWidth: 110
                    Layout.minimumWidth: 90
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Layout.preferredHeight: 935
                    Layout.preferredWidth: 100
                }

                Rectangle {
                    id: rectangle5

                    color: "#ffffff"
                    radius: 20
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Layout.preferredHeight: 935
                    Layout.preferredWidth: 1640
                }

                Rectangle {
                    id: rectangle4
                    color: "#ffffff"
                    radius: 20
                    Layout.rightMargin: 5
                    Layout.minimumWidth: 90
                    Layout.maximumWidth: 110
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.preferredHeight: 935
                    Layout.preferredWidth: 100
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.33}D{i:5}D{i:2}D{i:1}
}
##^##*/

